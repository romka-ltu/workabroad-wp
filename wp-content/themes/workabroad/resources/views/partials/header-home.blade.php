<header class="header-home">
  <div class="container-fluid text-white">
    <div class="flex items-center pt-26px relative">
      <a class="relative z-50 w-182px md:w-auto" href="{{ home_url('/') }}">
        <img src="@asset('images/logo-white.svg')" alt="">
      </a>
      <nav class="nav-primary flex-1 flex justify-end">
        <img src="@asset('images/menu.svg')" id="menutoggle" class="mobilemenu lg:hidden z-50" alt="">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
    </div>
    <div class="absolute container-fluid flex flex-col w-full h-full justify-center top-0 left-0 right-0 mx-auto">
      <h1 class="text-40px lg:text-60px font-bold">{!! $section_slider->title !!}</h1>
      <h3 class="text-20px lg:text-35px font-semibold">{!! $section_slider->subtitle !!}</h3>
    </div>
    <div class="arrow-down w-26px md:w-52px absolute bottom-0 left-0 right-0 mx-auto mb-16">
      <a href="#section_apie_mus" class="scrollto">
        <img src="@asset('images/arrow-down.svg')" alt="">
      </a>
    </div>
  </div>
</header>

<header class="header bg-brand">
  <div class="container-fluid text-white">
    <div class="flex items-center relative">
      <a class="relative z-50 w-182px md:w-auto" href="{{ home_url('/') }}">
        <img src="@asset('images/logo-white.svg')" alt="">
      </a>
      <nav class="nav-primary flex-1 flex justify-end">
        <img src="@asset('images/menu.svg')" id="menutoggle" class="mobilemenu lg:hidden z-50" alt="">
        @if (has_nav_menu('primary_navigation'))
          {!! wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']) !!}
        @endif
      </nav>
    </div>
  </div>
</header>

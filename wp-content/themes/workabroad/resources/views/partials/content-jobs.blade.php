<div class="shadow-lg relative rounded-lg mb-8 flex flex-col md:flex-row p-24px">
  <div class="md:w-170px">{!! get_the_post_thumbnail() !!}</div>
  <div class="flex flex-col lg:flex-row flex-1">
    <div class="flex-1 p-4">
      <h2 class="text-20px md:text-25px uppercase font-bold mb-3">{!! get_the_title() !!}</h2>
      <div class="flex text-18px md:text-20px">
        <div class="flex text-gray-2 mr-4">
          <img src="@asset('images/euro.svg')" alt="" class="mr-2">
          {{ get_field('atlyginimas') }}/h
        </div>
        <div class="flex text-gray-2">
          <img src="@asset('images/marker.svg')" alt="" class="mr-2"> {{ get_field('miestas')->name }}, {{ get_field('salis')->name }}
        </div>
      </div>
    </div>
    <div class="flex-1 p-4 lg:flex lg:flex-2">
      <div class="mb-4 lg:flex-1">
        <div class="text-20px md:text-25px uppercase font-bold mb-3">{{ __('DARBO APRAŠYMAS','wa') }}</div>
        <div class="text-gray-2 text-18px md:text-20px">{!! get_the_content() !!}</div>
      </div>
      <div class="lg:flex-1 lg:ml-8">
        <div>
          <div class="text-20px md:text-25px uppercase font-bold mb-3">{{ __('Reikalavimai','wa') }}</div>
          <div>
            <ul class="text-18px md:text-20px">
              @foreach( get_field('reikalavimai') as $req )
                <li class="flex items-center"><img src="@asset('images/elipse.svg')" class="mr-2" alt=""> {{ $req->name }}</li>
              @endforeach
            </ul>
          </div>
        </div>
      </div>
    </div>
  </div>

  <a href="javascript:"
     data-src="#apply"
     data-modal="true"
     data-fancybox
     data-postid="{{ get_the_ID() }}"
     class="apply_trigger mx-auto md:absolute right-0 bottom-0 md:mb-24px md:mr-24px block max-w-230px bg-brand text-18px md:text-20px uppercase text-center py-4 px-8 rounded-lg font-bold text-white mt-8">
    {{ __('PRETENDUOTI','wa') }}
  </a>
</div>

<footer class="footer bg-brand-1 text-white py-50px md:py-100px">
  <div class="container-fluid">
    <div class="flex flex-col lg:flex-row">
      <div class="flex-1 mb-12 lg:mb-0 lg:mr-12">
        <a class="block z-50 w-182px md:w-260px mb-8" href="{{ home_url('/') }}">
          <img src="@asset('images/logo-white.svg')" alt="">
        </a>
        <div class="max-w-336px">
          <div>Įsidarbinimo partneriai Olandijoje.</div>
          <div>Įdarbinimas nemokamas!</div>
        </div>
      </div>
      <div class="widget-area flex-2">
        @php dynamic_sidebar('sidebar-footer') @endphp
      </div>
    </div>
  </div>
</footer>

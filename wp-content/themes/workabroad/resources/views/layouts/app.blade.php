<!doctype html>
<html {!! get_language_attributes() !!}>
  @include('partials.head')
  <body @php body_class() @endphp>
    @php do_action('get_header') @endphp

    @if( is_front_page() || is_home() )
      @include('partials.header-home')
    @else
      @include('partials.header')
    @endif

    @yield('content')
    @php do_action('get_footer') @endphp
    @include('partials.footer')
    @php wp_footer() @endphp
    @yield('footer-js')
  </body>
</html>

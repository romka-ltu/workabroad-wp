@extends('layouts.app')

@section('header-css')
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.css" />
@stop

@section('footer-js')
  <script src="https://cdn.jsdelivr.net/gh/fancyapps/fancybox@3.5.7/dist/jquery.fancybox.min.js"></script>
@stop

@section('content')
  <div class="container-fluid py-50px lg:py-100px overflow-hidden">
    @include('partials.page-header')
    @while(have_posts()) @php the_post() @endphp
      @include('partials.content-'.get_post_type())
    @endwhile

    <div id="apply" class="relative" style="display: none;">
      @php echo do_shortcode("[ninja_form id='3']") @endphp
      <img src="@asset('images/close.svg')" data-fancybox-close class="close-btn absolute top-0 right-0 mr-12 mt-8" alt="">
    </div>

  </div>
@stop

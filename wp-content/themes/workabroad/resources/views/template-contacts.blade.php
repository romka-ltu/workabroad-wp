{{--
  Template Name: Kontaktai
--}}

@extends('layouts.app')

@section('content')
  @while(have_posts()) @php the_post() @endphp
  <div class="container-fluid py-50px lg:py-100px overflow-hidden">
    @include('partials.page-header')
    <div class="flex flex-wrap -mx-8">
      <div class="w-full lg:w-1/2 xl:w-1/3 px-8 pt-8 pb-12 lg:pt-0 lg:pb-0">
        <div class="rounded-lg shadow-lg px-40px py-62px">
          <div>
            <span class="font-bold text-20px">{{ __('EL. PAŠTAS','wa') }}</span> <a href="mailto:{{ $email }}">{{ $email }}</a>
          </div>
          <div>
            <span class="font-bold text-20px">{{ __('TEL. NR.','wa') }}</span> <a href="tel:{{ $phone }}">{{ $phone }}</a>
          </div>
          <a href="{{ $facebook }}" class="block max-w-200px bg-brand text-20px uppercase text-center py-2 rounded-lg font-bold text-white mt-8">FACEBOOK</a>
        </div>
      </div>
      <div class="flex-1 px-8">
        {!! get_the_content() !!}
        @php echo do_shortcode("[ninja_form id='1']") @endphp
      </div>
    </div>
  </div>
  @endwhile
@endsection

@extends('layouts.app')

@section('content')
  <div id="section_apie_mus" class="bg-white py-100px">
    <div class="container-fluid">
      <h2 class="uppercase font-bold text-main text-35px">{{ $section_apie_mus->title }}</h2>
      <p class="text-18px lg:text-20px">
        {!! $section_apie_mus->text !!}
      </p>
    </div>
  </div>

  <div class="bg-brand text-white text-center py-100px uppercase">
    <h2 class="text-35px font-bold mb-52px">ŠIUO METU TURIME @if( $acf_jc = get_field('job_count') ) {{ $acf_jc }} @else {{ $job_count }} @endif LAISVAS DARBO VIETAS!</h2>
    <a href="{{ get_post_type_archive_link('jobs') }}" class="flex items-center justify-center py-4 lg:py-24px px-50px bg-white max-w-310px mx-auto rounded-lg text-brand text-18px lg:text-20px font-bold">Darbo pasiūlymai</a>
  </div>

  <div class="bg-gray-1 py-50px lg:py-100px">
    <div class="container-fluid">
      <h2 class="uppercase font-bold text-main text-25px lg:text-35px mb-40px lg:mb-80px">{{ __('KODĖL VERTA RINKTIS MUS','wa') }}</h2>
      <div class="flex flex-wrap -mx-4">
        <div class="w-full md:w-1/2 lg:w-1/3 px-4 mb-8">
          <div class="flex items-center justify-center flex-col text-center bg-white p-40px rounded-lg min-h-390px">
            <div class="bg-brand text-white flex items-center justify-center w-114px h-114px rounded-lg">
              <img src="@asset('images/kodel-ico-1.svg')" alt="">
            </div>
            <h3 class="font-bold text-20px my-6 uppercase">ĮDARBINIMAS OLANDIJOJE</h3>
            <p class="text-18px lg:text-20px font-semibold max-w-400px">Įdarbiname Olandiškoje agentūroje su
              Olandiška darbo sutartimi ir socialinėmis garantijomis!</p>
          </div>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/3 px-4 mb-8">
          <div class="flex items-center justify-center flex-col text-center bg-white p-40px rounded-lg min-h-390px">
            <div class="bg-brand text-white flex items-center justify-center w-114px h-114px rounded-lg">
              <img class="w-62px" src="@asset('images/kodel-ico-2@2x.png')" alt="">
            </div>
            <h3 class="font-bold text-20px my-6 uppercase">APGYVENDINIMAS</h3>
            <p class="text-18px lg:text-20px font-semibold max-w-400px">Suteikiame apgyvendinimą po vieną arba du žmones kambaryje, tik su tos pačios tautybės žmonėmis viename name!</p>
          </div>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/3 px-4 mb-8">
          <div class="flex items-center justify-center flex-col text-center bg-white p-40px rounded-lg min-h-390px">
            <div class="bg-brand text-white flex items-center justify-center w-114px h-114px rounded-lg">
              <img class="w-62px" src="@asset('images/kodel-ico-3@2x.png')" alt="">
            </div>
            <h3 class="font-bold text-20px my-6 uppercase">TRANSPORTAS</h3>
            <p class="text-18px lg:text-20px font-semibold max-w-400px">Jeigu neturite savo automobilio, jį suteiksime ir leisime naudotis savo reikmėms be apribojimų.</p>
          </div>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/3 px-4 mb-8">
          <div class="flex items-center justify-center flex-col text-center bg-white p-40px rounded-lg min-h-390px">
            <div class="bg-brand text-white flex items-center justify-center w-114px h-114px rounded-lg">
              <img src="@asset('images/kodel-ico-4.svg')" alt="">
            </div>
            <h3 class="font-bold text-20px my-6 uppercase">DARBO VALANDOS</h3>
            <p class="text-18px lg:text-20px font-semibold max-w-400px">Visi mūsų darbo pasiūlymai tik nuo 40 darbo valandų per savaitę.</p>
          </div>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/3 px-4 mb-8">
          <div class="flex items-center justify-center flex-col text-center bg-white p-40px rounded-lg min-h-390px">
            <div class="bg-brand text-white flex items-center justify-center w-114px h-114px rounded-lg">
              <img src="@asset('images/kodel-ico-5.svg')" alt="">
            </div>
            <h3 class="font-bold text-20px my-6 uppercase">ATLYGINIMAS</h3>
            <p class="text-18px lg:text-20px font-semibold max-w-400px">Atlyginimas yra mokamas kas savaitę, Penktadieniais, į bet kokios šalies banko sąskaitą.</p>
          </div>
        </div>
        <div class="w-full md:w-1/2 lg:w-1/3 px-4 mb-8">
          <div class="flex items-center justify-center flex-col text-center bg-white p-40px rounded-lg min-h-390px">
            <div class="bg-brand text-white flex items-center justify-center w-114px h-114px rounded-lg">
              <img src="@asset('images/kodel-ico-6.svg')" alt="">
            </div>
            <h3 class="font-bold text-20px my-6 uppercase">MUMS JŪS RŪPITE</h3>
            <p class="text-18px lg:text-20px font-semibold max-w-400px">Atsiradus klausimams ar problemoms, visada galite skambinti, mes esame Olandijoje ir visada jums padėsime</p>
          </div>
        </div>
      </div>
    </div>
  </div>

  @if( have_rows('section_highlights') )
    <div class="bg-white py-40px md:py-85px text-center">
      <div class="container-fluid">
        <ul class="section-highlights">
          @while( have_rows('section_highlights') )
            <?php the_row(); ?>
            <li class="section-highlights-title">
              <div class="max-w-420px">{{ get_sub_field('title') }}</div>
            </li>
          @endwhile
        </ul>
      </div>
    </div>
  @endif
@endsection

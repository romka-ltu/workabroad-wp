export default {
  init() {
    const menutoggle = document.getElementById('menutoggle');
    const $apply_trigger = $('.apply_trigger');

    menutoggle.addEventListener('click', (e) => {
      if ( e.currentTarget.classList.contains('active') ) {
        e.currentTarget.classList.remove('active');
      } else {
        e.currentTarget.classList.add('active');
      }
    });

    $apply_trigger.on('click', function(){
      $('#nf-field-32').val( `/wp-admin/post.php?post=${$(this).data('postid')}&action=edit` );
    });

    $('a.scrollto').click(function( event ) {
      event.preventDefault();
      $('html, body').animate({ scrollTop: $($(this).attr('href')).offset().top }, 500);
    });

  },
  finalize() {
    // JavaScript to be fired on all pages, after page specific JS is fired
  },
};

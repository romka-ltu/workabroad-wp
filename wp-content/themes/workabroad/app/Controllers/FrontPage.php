<?php

namespace App\Controllers;

use Sober\Controller\Controller;

class FrontPage extends Controller
{
    protected $acf = true;

    public function jobCount()
    {
        return wp_count_posts( 'jobs' )->publish;
    }
}
